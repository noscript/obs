Format: 3.0 (native)
Source: vim
Binary: vim
Version: _DUMMY_
DEBTRANSFORM-RELEASE: 1
Standards-Version: 4.1.1
Build-Depends: autoconf, debhelper-compat (= 12), libacl1-dev, libcanberra-dev, libgpmg1-dev [linux-any], libgtk-3-dev, libperl-dev, libselinux1-dev [linux-any], libsodium-dev, libncurses-dev, libtool-bin <!nocheck>, procps <!nocheck>, python3-dev, ruby [!alpha !ia64] <!pkg.vim.noruby>, ruby-dev [!alpha !ia64] <!pkg.vim.noruby>, tcl-dev, libjpeg-dev, lsb-base, libxt-dev
Build-Depends-Indep: docbook-xml, ghostscript
Build-Conflicts: autoconf2.13
